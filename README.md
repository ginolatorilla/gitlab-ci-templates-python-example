# Gitlab CI Templates Python Example

Here is an example of a project that uses my CI templates for Python.

[Merge request #1](https://gitlab.com/ginolatorilla/gitlab-ci-templates-python-example/-/merge_requests/1) progressively
enables pipeline features. After it's merged, the following changes happen to the project:

1. The tag [v0.0.1](https://gitlab.com/ginolatorilla/gitlab-ci-templates-python-example/-/tags/v0.0.1) is created.
2. The release [0.0.1](https://gitlab.com/ginolatorilla/gitlab-ci-templates-python-example/-/releases/v0.0.1) is created.
3. The Python package at version [0.0.1](https://gitlab.com/ginolatorilla/gitlab-ci-templates-python-example/-/packages/20395237) is published to the project's package registry.
